const express = require('express');
const router = express.Router();

// Middleware that is specific to this router
router.use(function timeLog(req, res, next) {
  console.log('Time: ', Date.now());
  next();
});

// Define the about route
router.get('/tes', function (req, res) {
  res.send('tes');
});

module.exports = router;
