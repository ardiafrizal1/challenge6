const express = require('express');
const bodyParser = require('body-parser');
const fs = require('fs');
const users = [];
let posts = require('./views/db/posts.json');
const { sequelize, User, Histories, Biodata } = require('./models');
//const user_games = require('./models/user_game');

const app = express();

const router = require('./router');
app.use(router);

app.use(express.json());

/* 
GET all data
endpoint: http://localhost:3000/api/v1/posts for get all data, method -> GET
*/
app.get('/api/v1/posts', (req, res) => {
  res.status(200).json(posts);
});

app.get('/api/v1/posts/:id', (req, res) => {
  const post = posts.find((i) => i.id === +req.params.id);
  res.status(200).json(post);
});

app.use(bodyParser.urlencoded({ extended: true }));

app.use(express.static('public'));
app.use(express.static('views'));

app.set('view engine', 'ejs');

app.get('/dashboard', (req, res) => {
  const name = req.query.name;
  res.render('dashboard', {
    name,
  });
});

app.get('/', function (req, res) {
  res.render('body');
});
app.get('/body', function (req, res) {
  res.render('body');
});
app.get('/about', function (req, res) {
  res.render('about');
});
app.get('/rps', function (req, res) {
  res.render('rps');
});
app.get('/login', function (req, res) {
  res.render('login');
});

app.post('/api/v1/login', (req, res) => {
  const { name, password } = req.body;

  const users = JSON.parse(fs.readFileSync('./views/db/posts.json'));
  if (users.find((users) => users.name === name && users.password === password)) {
    res.redirect('/dashboard');
  } else {
    res.status(401).json({ error: 'wrong username or password' });
  }
  //users.push({ name, password });
});
//app.listen(3000, function () {
//console.log('Server running on port 3000');
//});
//get all  users
app.get('/users', (req, res) => {
  user_games.findAll().then((user_games) => {
    res.render('readuser', {
      user_games,
    });
  });
});
//get all  histori
app.get('/users', (req, res) => {
  Histories.findAll().then((user_game_history) => {
    res.render('readhistory', {
      user_game_history,
    });
  });
});
//get all  Biodata
app.get('/users', (req, res) => {
  Biodata.findAll().then((user_game_biodata) => {
    res.render('readbio', {
      user_game_biodata,
    });
  });
});

// Form input a user
app.get('/users/create', (req, res) => {
  res.render('users/create');
});

// Create a new user
app.post('/users', (req, res) => {
  User.create({
    username: req.body.username,
    password: req.body.password,
  }).then((user_games) => {
    res.send('User berhasil dibuat');
  });
});
// Form input a Bio
app.get('/users/createbio', (req, res) => {
  res.render('users/createbio');
});

// Create a new bio
app.post('/users', (req, res) => {
  Biodata.create({
    name: req.body.name,
    email: req.body.email,
    gender: req.body.gender,
    date_of_birth: req.body.date_of_birth,
    city: req.body.city,
  }).then((user_game_biodata) => {
    res.send('User berhasil dibuat');
  });
});

//Delete an user
app.get('/delete/{:uuid}', (req, res) => {
  User.findOne({
    where: { uuid: req.params.uuid },
  }).then((user_games) => {
    res.render('users/delete');
  });
});
//Delete an bio
app.get('/delete/{:uuid}', (req, res) => {
  Biodata.findOne({
    where: { uuid: req.params.uuid },
  }).then((user_game_biodata) => {
    res.render('users/delete');
  });
});

//Update user by uuid

app.get('/updateuser/(:uuid)', (req, res) => {
  User.findOne({
    where: { uuid: req.params.uuid },
  }).then((user_games) => {
    res.render('users/updateuser', {
      uuid: user_games.uuid,
      username: user_games.username,
      password: user_games.password,
    });
  });
});

//update user by uuid
app.post('/users/updateuser/(:uuid)', (req, res) => {
  User.update(
    {
      username: req.body.username,
      password: req.body.password,
    },
    {
      where: { uuid: req.params.uuid },
    }
  )
    .then((users) => {
      res.send('User berhasil diupdate');
    })
    .catch((err) => {
      res.status(500).json("Can't update an user");
    });
});
//Update biodata by uuid

app.get('/updatebiodata/(:uuid)', (req, res) => {
  Biodata.findOne({
    where: { uuid: req.params.uuid },
  }).then((user_game_biodata) => {
    res.render('users/update', {
      uuid: user_game_biodata.uuid,
      username: user_game_biodata.username,
      password: user_game_biodata.password,
    });
  });
});

//update biodata by uuid
app.post('/users/update/(:uuid)', (req, res) => {
  User.update(
    {
      username: req.body.username,
      password: req.body.password,
    },
    {
      where: { uuid: req.params.uuid },
    }
  )
    .then((users) => {
      res.send('User berhasil diupdate');
    })
    .catch((err) => {
      res.status(500).json("Can't update an user");
    });
});
//Server listen on port 3000
app.listen({ port: 3000 }, async () => {
  console.log('Server up on http://localhost:3000');
  await sequelize.authenticate();
  console.log('Database Connected!');
});
