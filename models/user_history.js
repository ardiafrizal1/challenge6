'use strict';
const { Model } = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class user_game_history extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  user_game_history.init(
    {
      win: DataTypes.STRING,
      lose: DataTypes.STRING,
      time_play: DataTypes.STRING,
      total_match: DataTypes.STRING,
      win_rate: DataTypes.STRING,
    },
    {
      sequelize,
      tableName: 'user_game_history',
      modelName: 'Histories',
    }
  );
  return user_game_history;
};
