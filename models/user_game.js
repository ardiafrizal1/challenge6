'use strict';
const { Model } = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class user_game extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  user_game.init(
    {
      uuid: {
        type: DataTypes.STRING,
        defaultValue: DataTypes.UUIDV4,
      },
      username: {
        type: DataTypes.STRING,
        allowNull: false,
        validate: {
          notNull: { msg: 'user must have username' },
          notEmpty: { msg: 'user must not be empty' },
        },
      },

      passwrod: {
        type: DataTypes.STRING,
        allowNull: false,
        validate: {
          notNull: { msg: 'user must have password' },
          notEmpty: { msg: 'user must not be empty' },
        },
      },
    },
    {
      sequelize,
      tableName: 'user_games',
      modelName: 'User',
    }
  );
  return user_game;
};
