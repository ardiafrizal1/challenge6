const { Histories } = require('./models');
Histories.create({
  win: 'a100',
  lose: 'a10',
  time_play: 'a100 hours',
  total_match: 'a1000',
  win_rate: 'a50%',
}).then((user_history) => {
  console.log(user_history);
});
